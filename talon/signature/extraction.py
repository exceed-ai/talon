# -*- coding: utf-8 -*-

from __future__ import absolute_import

import logging

import numpy
import regex as re

from talon.signature.bruteforce import get_signature_candidate
from talon.signature.constants import (CLASSIFIER_THRESHOLD,
                                       MAX_AVG_LINE_LENGTH,
                                       TOO_LONG_SIGNATURE_LINE_WORD)
from talon.signature.learning.featurespace import build_pattern, features
from talon.signature.learning.helpers import has_signature
from talon.utils import get_delimiter

log = logging.getLogger(__name__)

EXTRACTOR = None

# regex signature pattern for reversed lines
# assumes that all long lines have been excluded
RE_REVERSE_SIGNATURE = re.compile(r'''
# signature should consists of blocks like this
(?:
   # it could end with empty line
   e*
   # there could be text lines but no more than 2 in a row
   (te*){,2}
   # every block should end with signature line
   s
)+
''', re.I | re.X | re.M | re.S)


def _sigmoid_function(prob):
    prob *= -1
    numpy.exp(prob, prob)
    prob += 1
    numpy.reciprocal(prob, prob)
    return prob


def is_long_line(line):
    return len(line.split(" ")) > TOO_LONG_SIGNATURE_LINE_WORD


def is_only_contain_sender_or_long_line_feature(data):
    # contain sender feature is at the end of the features
    # long line feature is the second features
    data = data[0]
    only_contain_sender = data[-1] == 1 and all(data[:-1] == 0)
    only_contain_sender_and_long_line = data[-1] == 1 and data[0] == 0 and  data[1] == 1 and all(data[2:-1] == 0)
    return only_contain_sender or only_contain_sender_and_long_line


def is_signature_line(line, sender, classifier):
    '''Checks if the line belongs to signature. Returns True or False.'''
    if len(line.split(" ")) >= MAX_AVG_LINE_LENGTH:
        return False

    data = numpy.array(build_pattern(line, features(sender))).reshape(1, -1)

    if is_only_contain_sender_or_long_line_feature(data) and is_long_line(line):
        return False

    prob = classifier.decision_function(data)
    prob = _sigmoid_function(prob)
    return prob > CLASSIFIER_THRESHOLD


def extract(body, sender):
    """Strips signature from the body of the message.

    Returns stripped body and signature as a tuple.
    If no signature is found the corresponding returned value is None.
    """
    try:
        delimiter = get_delimiter(body)

        body = body.strip()

        if has_signature(body, sender):
            lines = body.splitlines()

            markers = _mark_lines(lines, sender)
            text, signature = _process_marked_lines_by_the_first_signature_line(lines, markers)

            if signature:
                text = delimiter.join(text)
                if text.strip():
                    return (text, delimiter.join(signature))
    except Exception:
        log.exception('ERROR when extracting signature with classifiers')

    return (body, None)


def _mark_lines(lines, sender):
    """Mark message lines with markers to distinguish signature lines.

    Markers:

    * e - empty line
    * s - line identified as signature
    * t - other i.e. ordinary text line

    >>> mark_message_lines(['Some text', '', 'Bob'], 'Bob')
    'tes'
    """
    global EXTRACTOR

    candidate = get_signature_candidate(lines)

    # at first consider everything to be text no signature
    markers = list('t' * len(lines))

    # mark lines starting from bottom up
    # mark only lines that belong to candidate
    # no need to mark all lines of the message
    for i, line in reversed(list(enumerate(candidate))):
        # markers correspond to lines not candidate
        # so we need to recalculate our index to be
        # relative to lines not candidate
        j = len(lines) - len(candidate) + i
        if not line.strip():
            markers[j] = 'e'
        elif (is_signature_line(line, sender, EXTRACTOR) or _is_whitelist_signature(line)) and not _is_blacklist_signature(line):
            markers[j] = 's'

    return "".join(markers)


def _is_whitelist_signature(line):
    # Facebook
    # Instagram
    # LinkedIn
    # Twitter
    return line.lower().strip() in ['facebook', 'instagram', 'linkedin', 'twitter', 'google+', 'blog']


def _is_blacklist_signature(line):
    black_list =['vacation', 'pto', 'out of office', 'sorry', 'no longer', 'please', 'interested']
    return any(x in line.lower().strip() for x in black_list)

def _process_marked_lines_by_the_first_signature_line(lines, markers):
    """extract message's marked lines to strip signature.
    extract signature by the first signature line

    >>> _process_marked_lines(['Some text', '', 'Bob'], 'tes')
    (['Some text', ''], ['Bob'])
    """
    if 's' in markers:
        first_signature_line = markers.index('s')
        if first_signature_line > 0:
            return (lines[:first_signature_line], lines[first_signature_line:])

    return (lines, None)

def _process_marked_lines(lines, markers):
    """Run regexes against message's marked lines to strip signature.

    >>> _process_marked_lines(['Some text', '', 'Bob'], 'tes')
    (['Some text', ''], ['Bob'])
    """
    # reverse lines and match signature pattern for reversed lines
    signature = RE_REVERSE_SIGNATURE.match(markers[::-1])
    if signature:
        return (lines[:-signature.end()], lines[-signature.end():])

    return (lines, None)
