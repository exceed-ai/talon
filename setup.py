#!/usr/bin/env python
# https://intranet.genesys.com/display/RP/Utilising+PureCloud+Artifactory+for+Software+Artifacts
# https://intranet.genesys.com/display/PureCloud/SecDev+-+Knowledge+Sharing+Sessions
from __future__ import absolute_import

from json import loads

from setuptools import find_packages, setup
from setuptools.command.install import install

with open('version_info.json', 'r') as version_file:
    version = loads(version_file.read())['version']


with open('requirements.txt', 'r') as requirements_file:
    install_requires = requirements_file.read().splitlines()


with open('test-requirements.txt', 'r') as test_requirements_file:
    # skip the first line: "-r requirements.txt"
    install_test_requires = test_requirements_file.read().splitlines()[1:]


class InstallCommand(install):
    user_options = install.user_options + [
        ('no-ml', None, "Don't install without Machine Learning modules."),
    ]

    boolean_options = install.boolean_options + ['no-ml']

    def initialize_options(self):
        install.initialize_options(self)
        self.no_ml = None

    def finalize_options(self):
        install.finalize_options(self)
        if self.no_ml:
            dist = self.distribution
            dist.packages=find_packages(exclude=[
                'tests',
                'tests.*',
                'talon.signature',
                'talon.signature.*',
            ])
            for not_required in ['numpy', 'scipy', 'scikit-learn==0.16.1']:
                dist.install_requires.remove(not_required)


setup(name='talon',
      version=version,
      description=("Mailgun library maintained by Exceed.ai"
                   "to extract message quotations and signatures."),
      long_description=open("README.rst").read(),
      author='Mailgun Inc. (Exceed.ai)',
      author_email='joy.chen@genesys.com',
      url='https://bitbucket.org/exceed-ai/talon/src/master/',
      license='APACHE2',
      cmdclass={
          'install': InstallCommand,
      },
      packages=find_packages(exclude=['tests', 'tests.*']),
      include_package_data=True,
      zip_safe=True,
      install_requires=install_requires,
      tests_require=install_test_requires,
      )
