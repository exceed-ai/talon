// vi: set syntax=groovy:
@Library('pipeline-library@master')
import com.genesys.jenkins.Service

final serviceBuild = new Service()
final common = new com.genesys.jenkins.PythonCommon()

pipeline {
  agent {
    node {
      label 'dev_mesos_v2||dev_shared_v2' // https://bitbucket.org/inindca/jslave/src/master/
    }
  }

  parameters {
    string(name: 'BRANCH_TO_BUILD', defaultValue: 'master', description: 'Which branch to build from.')
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '45'))
    timeout(time: 1, unit: 'HOURS')
    timestamps()
    disableConcurrentBuilds()
    quietPeriod(3600)
  }

  environment {
    BASE_VERSION = '1.4.5' // should be the same as version in version_info.json
    EMAIL_LIST = 'joy.chen@genesys.com'
  }

  stages {

    stage('Running Tests and Linting') {
      steps {
      script {
          common.lintAndTest("talon", "test-requirements.txt", [pythonVersion: '3.10', pipVersion: "24.0.0"])
        }
      }
    }

    stage('Build and Upload') {
      steps {
        sh "set -eo pipefail"
        script {
          common.buildAndUpload("version_info.json", env.BASE_VERSION, [pythonVersion: '3.10', pipVersion: "24.0.0", currentBranch: env.BRANCH_TO_BUILD, upload: true])
        }
      }
    }

  }

  post {
    // Always (post-deployment step) send emails for Unstable or Failed builds
    failure {
      emailext body: '$BUILD_URL failed', subject: '$JOB_NAME/$BUILD_NUMBER failed', to: EMAIL_LIST
    }
    unstable {
      emailext body: '$BUILD_URL is unstable', subject: '$JOB_NAME/$BUILD_NUMBER is unstable', to: EMAIL_LIST
    }
    /*
    success {
      emailext body: '$BUILD_URL is successful', subject: '$JOB_NAME/$BUILD_NUMBER is successful', to: EMAIL_LIST
    }
    */
  }
}
