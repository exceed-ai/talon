# coding:utf-8

from __future__ import absolute_import

import cchardet
import six

from talon import utils as u

from . import *


def test_get_delimiter():
    eq_('\r\n', u.get_delimiter('abc\r\n123'))
    eq_('\n', u.get_delimiter('abc\n123'))
    eq_('\n', u.get_delimiter('abc'))


def test_unicode():
    eq_(u'hi', u.to_unicode('hi'))
    eq_(type(u.to_unicode('hi')), six.text_type)
    eq_(type(u.to_unicode(u'hi')), six.text_type)
    eq_(type(u.to_unicode('привет')), six.text_type)
    eq_(type(u.to_unicode(u'привет')), six.text_type)
    eq_(u"привет", u.to_unicode('привет'))
    eq_(u"привет", u.to_unicode(u'привет'))
    # some latin1 stuff
    eq_(u"Versión", u.to_unicode(u'Versi\xf3n'.encode('iso-8859-2'), precise=True))


def test_detect_encoding():
    eq_('ascii', u.detect_encoding(b'qwe').lower())
    ok_(u.detect_encoding(
        u'Versi\xf3n'.encode('iso-8859-2')).lower() in [
            'iso-8859-1', 'iso-8859-2'])
    eq_('utf-8', u.detect_encoding(u'привет'.encode('utf8')).lower())
    # fallback to utf-8
    with patch.object(u.chardet, 'detect') as detect:
        detect.side_effect = Exception
        eq_('utf-8', u.detect_encoding('qwe'.encode('utf8')).lower())


def test_quick_detect_encoding():
    eq_('ascii', u.quick_detect_encoding(b'qwe').lower())
    ok_(u.quick_detect_encoding(
        u'Versi\xf3n'.encode('windows-1252')).lower() in [
            'windows-1252', 'windows-1250'])
    eq_('utf-8', u.quick_detect_encoding(u'привет'.encode('utf8')).lower())


@patch.object(cchardet, 'detect')
@patch.object(u, 'detect_encoding')
def test_quick_detect_encoding_edge_cases(detect_encoding, cchardet_detect):
    cchardet_detect.return_value = {'encoding': 'ascii'}
    eq_('ascii', u.quick_detect_encoding(b"qwe"))
    cchardet_detect.assert_called_once_with(b"qwe")

    # fallback to detect_encoding
    cchardet_detect.return_value = {}
    detect_encoding.return_value = 'utf-8'
    eq_('utf-8', u.quick_detect_encoding(b"qwe"))

    # exception
    detect_encoding.reset_mock()
    cchardet_detect.side_effect = Exception()
    detect_encoding.return_value = 'utf-8'
    eq_('utf-8', u.quick_detect_encoding(b"qwe"))
    ok_(detect_encoding.called)


def test_html_to_text():
    html = """<body>
<p>Hello world!</p>
<br>
<ul>
<li>One!</li>
<li>Two</li>
</ul>
<p>
Haha
</p>
</body>"""
    text = u.html_to_text(html)
    eq_(b"Hello world! \n\n  * One! \n  * Two \nHaha", text)
    eq_(u"привет!", u.html_to_text("<b>привет!</b>").decode('utf8'))

    html = '<body><br/><br/>Hi</body>'
    eq_(b'Hi', u.html_to_text(html))

    html = """Hi
<style type="text/css">

div, p, li {

font: 13px 'Lucida Grande', Arial, sans-serif;

}
</style>

<style type="text/css">

h1 {

font: 13px 'Lucida Grande', Arial, sans-serif;

}
</style>"""
    eq_(b'Hi', u.html_to_text(html))

    html = """<div>
<!-- COMMENT 1 -->
<span>TEXT 1</span>
<p>TEXT 2 <!-- COMMENT 2 --></p>
</div>"""
    eq_(b'TEXT 1 \nTEXT 2', u.html_to_text(html))


def test_comment_no_parent():
    s = b'<!-- COMMENT 1 --> no comment'
    d = u.html_document_fromstring(s)
    eq_(b"no comment", u.html_tree_to_text(d))


@patch.object(u.html5parser, 'fromstring', Mock(side_effect=Exception()))
def test_html_fromstring_exception():
    eq_(None, u.html_fromstring("<html></html>"))


@patch.object(u, 'html_too_big', Mock())
@patch.object(u.html5parser, 'fromstring')
def test_html_fromstring_too_big(fromstring):
    eq_(None, u.html_fromstring("<html></html>"))
    assert_false(fromstring.called)


@patch.object(u.html5parser, 'document_fromstring')
def test_html_document_fromstring_exception(document_fromstring):
    document_fromstring.side_effect = Exception()
    eq_(None, u.html_document_fromstring("<html></html>"))


@patch.object(u, 'html_too_big', Mock())
@patch.object(u.html5parser, 'document_fromstring')
def test_html_document_fromstring_too_big(document_fromstring):
    eq_(None, u.html_document_fromstring("<html></html>"))
    assert_false(document_fromstring.called)


@patch.object(u, 'plain_text_too_big', Mock())
@patch.object(u.html5parser, 'document_fromstring')
def test_html_document_fromstring_too_big_with_plain_text(document_fromstring):
    eq_(None, u.html_document_fromstring("<html></html>"))
    assert_false(document_fromstring.called)


@patch.object(u, 'html_fromstring', Mock(return_value=None))
def test_bad_html_to_text():
    bad_html = "one<br>two<br>three"
    eq_(None, u.html_to_text(bad_html))


@patch.object(u, '_MAX_TAGS_COUNT', 3)
def test_html_too_big():
    eq_(False, u.html_too_big("<div></div>"))
    eq_(True, u.html_too_big("<div><span>Hi</span></div>"))


@patch.object(u, '_MAX_WORDS_COUNT', 3)
@patch.object(u, '_MIN_TAGS_COUNT', 4)
def test_plain_text_too_big():
    eq_(True, u.plain_text_too_big("<div>Hi this is a long plain text</div>"))
    eq_(False, u.plain_text_too_big("<div>short plain text</div>"))
    eq_(False, u.plain_text_too_big("<div><span>Hi this is a valid html</span></div>"))


@patch.object(u, '_MAX_TAGS_COUNT', 3)
def test_html_to_text():
    eq_(b"Hello", u.html_to_text("<div>Hello</div>"))
    eq_(None, u.html_to_text("<div><span>Hi</span></div>"))


@patch.object(u, '_MAX_WORDS_COUNT', 1000)
@patch.object(u, '_MIN_TAGS_COUNT', 10)
def test_long_history_html_will_not_classify_as_plain_text_too_big():
    eq_(False, u.plain_text_too_big("<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<meta content=\"text/html; charset=utf-8\">\r\n</head>\r\n<body>\r\n<div>\r\n<div dir=\"auto\">I did, we already use Petah Tikva and we are pleased so far</div>\r\n</div>\r\n<div dir=\"auto\">Thank You</div>\r\n<div dir=\"auto\"><br>\r\n</div>\r\n<div><br>\r\n<div class=\"gmail_quote\">\r\n<div dir=\"ltr\" class=\"gmail_attr\">On Fri, Oct 18, 2019 at 10:50 AM Jenny Pitsos &lt;<a href=\"mailto:jenny@petahtikvaus.com\">jenny@petahtikvaus.com</a>&gt; wrote:<br>\r\n</div>\r\n<blockquote class=\"gmail_quote\" style=\"margin:0 0 0 .8ex; border-left:1px #ccc solid; padding-left:1ex\">\r\n<div><img src=\"https://prod.exceed.ai/api/external/callback/track.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2R1bGUiOiJUcmFja0VtYWlsT3BlbmluZ0NhbGxiYWNrTW9kdWxlIiwiaXNzIjoiYXV0aDAiLCJjb21tSWQiOiI1ZDg3NjliOTE4ZDY4MTQyYmNjZTVjYjgiLCJleHAiOjE1NzY1OTc4MDB9.WSAMc-sFv--Km9P2128Oombm-o7Sz3RA01RgqUP345U\"><br>\r\n<p></p>\r\n<p><span style=\"color:#000000\">Ralph, </span></p>\r\n<p><span style=\"color:#000000\"><span style=\"background-color:#ffffff\"><span style=\"font-size:14px\">Just checking in to see if you received my last email by chance and whether you’d be interested to chat about our factoring options and other services that benefit\r\n your business?</span></span></span></p>\r\n<p><span style=\"color:#000000\"><span style=\"background-color:#ffffff\"><span style=\"font-size:14px\">Looking forward to helping you build your business.\r\n</span></span></span></p>\r\n<p><span style=\"color:#000000\"><span style=\"font-size:14px\"><span style=\"background-color:#ffffff\">Damos la bienvenida a las respuestas o preguntas en español. ¡Tenemos múltiples gestores de cuentas bilingües!</span></span></span></p>\r\n<p><span style=\"color:#000000\"><span style=\"background-color:#ffffff\"><span style=\"font-size:14px\">Kind regards,\r\n</span></span></span></p>\r\n<p><span style=\"color:#000000\"><span style=\"background-color:#ffffff\"><span style=\"font-size:14px\"></span></span></span></p>\r\n<p style=\"text-align:start\"></p>\r\n<div style=\"float:left\"><img src=\"http://res.cloudinary.com/exceed/image/upload/v1547381883/WebUpload/nfvl8aot6eqd7mep379p.png\"></div>\r\n<p><strong>Jenny L. Pitsos</strong><br>\r\nSales Coordinator &amp; Team Lead<br>\r\nPetah TikvaUS Capital, LLC<br>\r\nMarietta, GA | Picayune, MS | Shelby, NC | Cincinnati, OH<br>\r\nPh: 909-272-1885<br>\r\n<a href=\"http://www.petahtikvaus.com/\" target=\"_blank\">www.petahtikvaus.com</a></p>\r\n<div style=\"text-align:left\"><img src=\"http://res.cloudinary.com/exceed/image/upload/v1547473756/WebUpload/uxidwd1x79ypsmdwe6wi.png\"></div>\r\n<p></p>\r\n<div style=\"font-size:12px; color:grey; clear:both\">If you don't want to get any more emails from us, simply respond with unsubscribe or click\r\n<a href=\"https://prod.exceed.ai/api/external/callback?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2R1bGUiOiJVbnN1YnNjcmliZUNhbGxiYWNrTW9kdWxlIiwiaXNzIjoiYXV0aDAiLCJleHAiOjE3MjU5Nzc5NDIsInVzZXJJZCI6IjVkN2E1M2M0ZjIwYTVhM2ViNTU2OTc1OSJ9.oNpjcbFqzp-U_G2O5eAFZq1LoqYS5124Pk7C7YXZeLU&amp;commId=5d8769b918d68142bcce5cb8\" target=\"_blank\">\r\nUnsubscribe</a></div>\r\n<p></p>\r\n<br>\r\n<br>\r\n<p>On Thu, Sep 12, 2019, at 2:19 PM, Jenny Pitsos wrote:</p>\r\n<p></p>\r\n<blockquote><img src=\"https://prod.exceed.ai/api/external/callback/track.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2R1bGUiOiJUcmFja0VtYWlsT3BlbmluZ0NhbGxiYWNrTW9kdWxlIiwiaXNzIjoiYXV0aDAiLCJjb21tSWQiOiI1ZDdhNTNkNjE4ZDY4MTMyOTI4YTIyNDMiLCJleHAiOjE1NzQwNzkxMjB9.xrCVybPSdoxPXPX8AFCXgwRTt4MAR5qLsRS-Gh8cZuw\"><br>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#000000\">Ralph, </span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#333333\"><span style=\"background-color:#ffffff\">We are small factoring company in Marietta, GA called Petah Tikva Capital. We are the opposite of the large factoring companies. No tricks, no teaser rates, no hidden fees or long complicated\r\n contracts like we see so often from the big factoring companies. </span></span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#333333\">We take a lot of pride in offering exceptional customer service to our clients, we assign one dedicated account manager to your company who handles all of your day to day funding, invoicing, and collections.\r\n</span><span style=\"font-size:14px\"><span style=\"background-color:#ffffff\"><span style=\"color:#333333\">In addition, we are happy to help\r\n</span><span style=\"color:#000000\">you with your business, from connecting with brokers, load boards, insurance, dispatchers, etc.</span></span></span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#333333\">Here are some quick bullet points about our services:</span></p>\r\n<ul>\r\n<li><strong><em>NO sign-up fees, NO monthly minimum volume, NO reserve </em></strong></li><li><strong><em><span style=\"color:#333333\">Simple three-page factoring agreement</span></em></strong></li><li><strong><em><span style=\"color:#333333\">Fuel Advances on your picked-up loads until 10pm M-F and Advances available on Saturday!</span></em></strong></li><li><strong><em><span style=\"color:#333333\">EFS Fuel Card available with discount up to 30 cents per gallon</span></em></strong></li><li><strong><em><span style=\"color:#333333\">We handle all your invoicing, collection calls, and provide access to check the credit of the brokers you work with</span></em></strong></li></ul>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#333333\">On the freight side of things, we are partnered with several large brokerage companies with additional opportunities for your company. We factor these brokers at a rate of\r\n<em>1.5%</em> - with no reserve.</span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#333333\">Please reach out to our team so we can help!</span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#000000\"><em>Damos la bienvenida a las respuestas o preguntas en español. ¡Tenemos múltiples gestores de cuentas bilingües!</em></span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n<span style=\"color:#333333\">Kind regards, </span></p>\r\n<p style=\"display:block; margin-top:1em; margin-bottom:1em; margin-left:0; margin-right:0\">\r\n</p>\r\n<p style=\"text-align:start\"></p>\r\n<div style=\"float:left\"><img src=\"http://res.cloudinary.com/exceed/image/upload/v1547381883/WebUpload/nfvl8aot6eqd7mep379p.png\"></div>\r\n<p><strong>Jenny L. Pitsos</strong><br>\r\nSales Coordinator &amp; Team Lead<br>\r\n Petah Tikva US Capital, LLC<br>\r\nMarietta, GA | Picayune, MS | Shelby, NC | Cincinnati, OH<br>\r\nPh: 909-272-1885<br>\r\n<a href=\"http://www.petahtikvaus.com/\" target=\"_blank\">www.petahtikvaus.com</a></p>\r\n<div style=\"text-align:left\"><img src=\"http://res.cloudinary.com/exceed/image/upload/v1547473756/WebUpload/uxidwd1x79ypsmdwe6wi.png\"></div>\r\n<p></p>\r\n<div style=\"font-size:12px; color:grey; clear:both\">If you don't want to get any more emails from us, simply respond with unsubscribe or click\r\n<a href=\"https://prod.exceed.ai/api/external/callback?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2R1bGUiOiJVbnN1YnNjcmliZUNhbGxiYWNrTW9kdWxlIiwiaXNzIjoiYXV0aDAiLCJleHAiOjE3MjU5Nzc5NDIsInVzZXJJZCI6IjVkN2E1M2M0ZjIwYTVhM2ViNTU2OTc1OSJ9.oNpjcbFqzp-U_G2O5eAFZq1LoqYS5124Pk7C7YXZeLU&amp;commId=5d7a53d618d68132928a2243\" target=\"_blank\">\r\nUnsubscribe</a></div>\r\n</blockquote>\r\n<p></p>\r\n</div>\r\n</blockquote>\r\n</div>\r\n</div>\r\n</body>\r\n</html>\r\n"))

@patch.object(u, '_MAX_WORDS_COUNT', 1000)
@patch.object(u, '_MIN_TAGS_COUNT', 10)
def test_normal_html_will_not_classify_as_plain_text_too_big():
    eq_(False, u.plain_text_too_big("<p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><span style=\"color:#000000\">Hi Laurie,</span></p><div><span style=\"background-color:#ffffff\"><span style=\"color:#000000\">Institutions that use Ayalon SONIS are able to </span></span>make data-based decisions, engage students and applicants with automated communications, securely manage student data and academic records, as well as streamline and automate manual tasks.</div><br/><div>AyalonSONIS enables you to provide the high level of personal attention and service your students demand.</div><br/><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><span style=\"color:#000000\">If this is something that you&#x27;d be interested in, would</span> <span style=\"color:#000000\">next week work for a quick call? I would love to share how our product will make your life and your students&#x27; lives so much easier!</span></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><span style=\"color:#000000\">Looking forward to connecting with you.</span></p><div><span style=\"color:#000000\">Best,</span></div><br/><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><span style=\"color:#000000\"><div><strong>Sophia Mike</strong></div><div>Sales Development Rep<br/>Ayalon<br/>P: 617-492-9099 </div><div class=\"media-wrap image-wrap\"><a style=\"display:inline-block\" href=\"www.jenzabar.com\" target=\"_blank\"><img class=\"media-wrap image-wrap\" src=\"http://res.cloudinary.com/exceed/image/upload/v1569265603/WebUpload/bxxyjvdeedhly4ph4ood.png\" width=\"181px\" height=\"43px\" style=\"width:181px;height:43px\"/></a></div><br/></span></p><div style=\"font-size: 9px; color:grey; clear:both;\"><div>If you don&#x27;t want to get any more emails from us, simply respond with unsubscribe or click <a href='https://prod.exceed.ai/api/external/callback?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2R1bGUiOiJVbnN1YnNjcmliZUNhbGxiYWNrTW9kdWxlIiwiaXNzIjoiYXV0aDAiLCJleHAiOjE3MjczMDg4MTksInVzZXJJZCI6IjVkODM5OGZiMThkNjgxNDIwMDM3NzI3MSJ9.7EkRbNGnGv3UY6vuLc9ErrjC49L662bAEZWq5LfV1EA&commId=000000000000000000000000' target='_blank' style='color:grey'>here</a></div></div>"))
    eq_(False, u.plain_text_too_big("<p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><span style=\"color:#000000\">Rasul, </span></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\">Just wanted to touch base and see if you were still interested in discussing what Petah Tikva has to offer. </p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\">Here are some quick bullet points about our services:</p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>1.      NO sign-up fees, NO monthly minimum volume</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>2.      We can fund using faxed and emailed BOLs if the broker or customer does not require originals</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>3.      We accept paperwork up to 2:30 daily (Eastern Time) for ACH direct deposit funding.</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>4.      Fuel Advances on your picked-up loads until 5pm M-F and Advances available on Saturday!</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>5.      Funding options with no reserve</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>6.      Free Credit Check</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>7.      Free consultancy (help finding loads, equipment/trucks/trailers, etc)</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>8.      EFS Fuel Card available</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><em>9.      We handle all your invoicing, collection calls, and provide access to check the credit of the brokers you work with</em></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\">Looking forward to chatting!!</p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\">Kind regards, </p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><p style=\"text-align:start;\"></p><div class=\"media-wrap image-wrap float-left\" style=\"float:left\"><img class=\"media-wrap image-wrap float-left\" src=\"http://res.cloudinary.com/exceed/image/upload/v1547381883/WebUpload/nfvl8aot6eqd7mep379p.png\"/></div><p><strong>Jenny L. Pitsos</strong><br/>Sales Coordinator &amp; Team Lead<br/>Petah Tikva US Capital, LLC<br/>Marietta, GA | Picayune, MS | Shelby, NC | Cincinnati, OH<br/>Ph: 909-272-1885<br/><a href=\"http://www.petahtikvaus.com/\" target=\"_blank\">www.petahtikvaus.com</a></p><div class=\"media-wrap image-wrap align-left\" style=\"text-align:left\"><img class=\"media-wrap image-wrap align-left\" src=\"http://res.cloudinary.com/exceed/image/upload/v1547473756/WebUpload/uxidwd1x79ypsmdwe6wi.png\"/></div></p><p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"></p>"))
    eq_(False, u.plain_text_too_big("<p style=\"display: block; margin-top: 1em; margin-bottom: 1em; margin-left: 0; margin-right: 0;\"><span style=\"color:#003ba5\"><span style=\"font-family:Arial, Helvetica, sans-serif\">Hi Jessica,</span></span></p><br/><div><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"color:#003ba5\">I wanted to follow up and share a video on how or Buy Now solution works. </span><a href=\"https://www.youtube.com/watch?v=4O4Ugej388Xmnso&amp;t=9s\">https://www.youtube.com/watch?v=4O4Uf37ejemvo&amp;t=9s</a></span></div><br/><div><span style=\"color:#003ba5\"><span style=\"font-family:Arial, Helvetica, sans-serif\">I&#x27;d be keen to learn how you think this could impact your strategy.</span></span></div><br/><br/><br/><div><span style=\"color:#003ba5\"><span style=\"font-family:Arial, Helvetica, sans-serif\"><p><strong>Best Regards,</strong></p><p><strong>Jeff Walsh</strong><br/><strong>Global Brand Consultant.</strong><br/>We make the world instantly shoppable<br/>www.happychannel.com<br/><a href=\"https://www.happychannel.com/book-demo\" target=\"_blank\">Book a demo!</a></p></span></span></div>"))
    eq_(True, u.plain_text_too_big("<p>Hi, I really don&#39;t care about your problems I don&#39;t know how to handle your request I hate you stop yelling at me I&#39;m going to hang up I understand we are not in the same page The dead trees waited to be ignited by the smallest spark and seek their revenge The llama couldn&#39;t resist trying the lemonade You&#39;re good at English when you know the difference between a man eating chicken and a man-eating chicken The llama couldn&#39;t resist trying the lemonade You&#39;re good at English when you know the difference between a man eating chicken and a man-eating chicken Tomatoes make great weapons when water balloons aren’t available Although it wasn&#39;t a pot of gold, Nancy was still enthralled at what she found at the end of the rainbow The hawk didn’t understand why the ground squirrels didn’t want to be his friend Although it wasn&#39;t a pot of gold, Nancy was still enthralled at what she found at the end of the rainbow When a tree falls in the forrest but noone is there to hear it, does it make any sound? Time flies like an arrow She sells sea-shells at the sea shore The fish boat got out of the fish net to raise a pet Got get it going otherwise I&#39;m bowing When you got to shoot, shoot, don&#39;t talk Talking is like walking but with a tee Basketball is a team sport in which two teams most commonly of five players each opposing one another on a rectangular court compete with the primary objective of shooting a basketball through the defender&#39;s hoop while preventing the opposing team from shooting through their own hoop. A field goal is worth two points unless made from behind the three-point line when it is worth three. After a foul, timed play stops and the player fouled or designated to shoot a technical foul is given one, two or three one-point free throws. The team with the most points at the end of the game wins but if regulation play expires with the score tied, an additional period of play (overtime) is mandated. Players advance the ball by bouncing it while walking or running (dribbling) or by passing it to a teammate both of which require considerable skill. On offense, players may use a variety of shots – the layup, the jump shot, or a dunk; on defense, they may steal the ball from a dribbler, intercept passes, or block shots; either offense or defense may collect a rebound, that is, a missed shot that bounces from rim or backboard. It is a violation to lift or drag one&#39;s pivot foot without dribbling the ball, to carry it, or to hold the ball with both hands then resume dribbling. The five players on each side fall into five playing positions.  The tallest player is usually the center the second-tallest and strongest is the power forward a slightly shorter but more agile player is the small forward the shortest players or the best ball handlers are the shooting guard and the point guard who implements the coach&#39;s game plan by managing the execution of offensive and defensive plays (player positioning). Informally, players may play three-on-three, two-on-two, and one-on-one. She folded her handkerchief neatly He wondered if she would appreciate his toenail collection Shingle color was not something the couple had ever talked about They desperately needed another drummer since the current one only knew how to play bongos Shingle color was not something the couple had ever talked about They desperately needed another drummer since the current one only knew how to play bongos Never underestimate the willingness of the greedy to throw you under the bus Hang on, my kittens are scratching at the bathtub and they&#39;ll upset by the lack of biscuits The skeleton had skeletons of his own in the closet You should never take advice from someone who thinks red paint dries quicker than blue paint She found his complete dullness interesting There&#39;s a growing trend among teenagers of using frisbees as go-cart wheels He enjoys practicing his ballet in the bathroom The skeleton had skeletons of his own in the closet You should never take advice from someone who thinks red paint dries quicker than blue paint She found his complete dullness interesting There&#39;s a growing trend among teenagers of using frisbees as go-cart wheels He enjoys practicing his ballet in the bathroom Fluffy pink unicorns are a popular status symbol among macho men The best part of marriage is animal crackers with peanut butter She had a difficult time owning up to her own crazy self The best part of marriage is animal crackers with peanut butter She had a difficult time owning up to her own crazy self As he entered the church he could hear the soft voice of someone whispering into a cell phone David proudly graduated from high school top of his class at age 97 Two more days and all his problems would be solved Choosing to do nothing is still a choice, after all Two more days and all his problems would be solved Choosing to do nothing is still a choice, after all The miniature pet elephant became the envy of the neighborhood Malls are great places to shop, sleep, stroll, hike, walk and run; I can find everything I need under one roof Before he moved to the inner city, he had always believed that security complexes were psychological Everything was going so well until I was accosted by a purple giraffe Pink horses galloped across the sea Before he moved to the inner city, he had always believed that security complexes were psychological I really just need three more phrases To reach a 100 phrases for this transcript So now I&#39;m done, yay! Everything was going so well until I was accosted by a purple giraffe Pink horses galloped across the sea Some bathing suits just shouldn’t be worn by some people Being unacquainted with the chief raccoon was harming his prospects for promotion If my calculator had a history, a lot of history, it would be more embarrassing than my browser history The paintbrush was angry at the color the artist chose to use I like to leave work after my eight-hour tea-break and sea breake, right now it would be more</p>"))